const express = require('express')
const app = express()
const graphqlHTTP = require('express-graphql')
const { buildSchema } = require('graphql')
const axios = require('axios')
const cors = require('cors')
const url = 'https://api.github.com/graphql'
const query = `query {
    repository(owner: "facebook", name: "react") {
        issues(last: 5) {
            nodes {
                title
                number
                author {
                    login
                }
                comments(first: 5) {
                    nodes {
                        author {
                            login
                        }
                        bodyHTML
                    }
                }
            }
        }
    }
}`

var schema = buildSchema(`
    type Query {
        issues: [IssuesNodes]
    }

    type IssuesNodes {
        title: String
        number: String
        author: Author
        comments: Comments
    }

    type Comments {
        nodes: [CommentsNodes]
    }

    type Author {
        login: String
    }

    type CommentsNodes {
        bodyHTML: String
        author: Author
    }
`)

async function getGHIssue() {
    try {
        const result = await axios({
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'bearer 1fef668b29352886d9f885099afd734fed11f4ad'
            },
            data: {
                query
            }
        })
        return result.data.data.repository.issues.nodes;
    } catch (e) {
        console.log(e)
    }
}

const root = {
    issues: async () => {
        return await getGHIssue();
    }
}

app.use(cors())
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}))
app.listen(3000);